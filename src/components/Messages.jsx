import React, { useState, useContext } from "react";
import MainContext from "../MainContext";

const Messages = () => {
    const {messages, addMessage} = useContext(MainContext);
    let [input, setInput] = useState('');

    const handleChange = (e) => {
        setInput(e.target.value);
    };
    
    const handleClick = () => {
        addMessage(input);
    };

    return (
        <div>
            <input value={input} onChange={handleChange}/>
            <button onClick={handleClick}>Send</button>
            <ul>{messages.map((message, id) => <li key={id}>{message}</li>)}</ul>
        </div>
    );
}

export default Messages;