import { Route } from 'react-router';
import { BrowserRouter, NavLink } from 'react-router-dom';
import s from './App.css';
import Home from './components/Home';
import Main from './components/Main';
import Messages from './components/Messages';
import { ContextProvider } from './MainContext';

const App = (props) => {
  const messages = [1, 2, 3];

  const addMessage = (message) => {
    messages.push(message);
    return messages;
  };

  return (
    <BrowserRouter>
    <ContextProvider value={{messages, addMessage}}>
      <div>
        <div className='nav'>
            <ul>
              <li>
                <NavLink to='/home' activeClassName={s.active}>Home</NavLink>
              </li>
              <li>
                <NavLink to='/main'>Main</NavLink>
              </li>
              <li>
                <NavLink to='/messages'>Messages</NavLink>
              </li>
            </ul>
          </div>

        <Route component={Home} path='/home'/>
        <Route component={Main} path='/main'/>
        <Route component={Messages} path='/messages'/>
      </div>
      </ContextProvider>
    </BrowserRouter>
  );
}

export default App;